#ifndef __LIST_H__
#define __LIST_H__

#include "stdlib.h"
#include "collection.h"


/// TYPES ///
typedef
struct ListNode {
        void ** value;
        struct ListNode * next;
        struct ListNode * previous;
}
ListNode;

typedef
struct List {
        size_t size;
        struct ListNode * firstNode;
        struct ListNode * lastNode;
}
List;


/// CONSTANTS ///
extern const size_t LIST_NODE_SIZE;
extern const size_t LIST_SIZE;


/// FUNCTIONS ///

/**
 * @brief Creates a new list node.
 * @param value The value of the node.
 * @return The new list node.
 */
ListNode * new_list_node(void * value);

/**
 * @brief Creates a new list.
 * @return The new list.
 */
List * new_list();

/**
 * @brief Adds a new element to the list.
 * @param list The list.
 * @param value The value to add.
 * @return The number of elements in the list.
 */
size_t list_push(List * list, ListNode * node);

/** list_insert
 * @brief Inserts a node at the given index.
 * @param list The list.
 * @param node The node to insert.
 * @param index The index.
 * @return The number of elements in the list.
 */
size_t list_insert(List * list, ListNode * node, index_t index);

/**
 * @brief Gets the element at the given index.
 * @param list The list.
 * @param index The index.
 * @return The element at the given index or NULL if the index is out of bounds.
 */
ListNode * list_get_node(List * list, index_t index);

/**
 * @brief Removes the first element from the list.
 * @param list The list.
 * @return The removed element or NULL if the list is empty.
 */
ListNode * list_shift(List * list);

/**
 * @brief Removes and returns the last element of the list.
 * @param list The list.
 * @return The removed element or NULL if the list is empty.
 */
ListNode * list_pop(List * list);

/**
 * @brief Frees the given node.
 * @param node The node.
 */
void free_list_node(ListNode * node);

/**
 * @brief Frees the given list.
 * @param list The list.
 */
void free_list(List * list);

#endif // __LIST_H__
