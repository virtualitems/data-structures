#include "memory.h"
#include "string.h"

void * allocate(size_t size) {
        if (size <= 0) {
                return NULL;
        } else {
                return malloc(size);
        }
}
