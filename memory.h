#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "stdlib.h"


/**
 * @brief Allocates memory.
 * @param size The size of the memory to allocate.
 * @return The allocated memory.
 */
void * allocate(size_t size);


#endif // __MEMORY_H__