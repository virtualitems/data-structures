#include "memory.h"
#include "collection.h"
#include "list.h"


/// CONSTANTS ///
const size_t LIST_NODE_SIZE = sizeof(ListNode);
const size_t LIST_SIZE = sizeof(List);


/// FUNCTIONS ///
ListNode * new_list_node(void * value) {
        ListNode * node = (ListNode *) allocate(LIST_NODE_SIZE);
        node->value = value;
        node->next = NULL;
        node->previous = NULL;
        return node;
}

List * new_list() {
        List * list = (List *) allocate(LIST_SIZE);
        list->size = 0;
        list->firstNode = NULL;
        list->lastNode = NULL;
        return list;
}

size_t list_push(List * list, ListNode * node) {
        if (list->firstNode == NULL) {
                list->firstNode = node;
                list->lastNode = node;
        }
        else {
                node->previous = list->lastNode;
                node->next = NULL;
                list->lastNode->next = node;
                list->lastNode = node;
        }
        return ++list->size;
}

size_t list_insert(List * list, ListNode * node, index_t index) {
        if (index < 0 || index > list->size) {
                return 0;
        }
        if (index == list->size) {
                // push at end
                return list_push(list, node);
        }
        if (index == 0) {
                // [new] [first]
                node->next = list->firstNode;
                node->previous = NULL;
                list->firstNode->previous = node;
                list->firstNode = node;
                return ++list->size;
        }
        else {
                ListNode * nodeInPosition = list_get_node(list, index);
                ListNode * previousNodeInPosition = nodeInPosition->previous;
                ListNode * nextNodeInPosition = nodeInPosition->next;
                // [prev] [new] [in] [next]
                previousNodeInPosition->next = node;
                node->previous = previousNodeInPosition;
                node->next = nodeInPosition;
                nodeInPosition->previous = node;
                nodeInPosition->next = nextNodeInPosition;
                if (nextNodeInPosition != NULL) {
                        nextNodeInPosition->previous = nodeInPosition;
                }
                return ++list->size;
        }
}

ListNode * list_get_node(List * list, index_t index) {
        if (index >= list->size || index < 0) {
                return NULL;
        }
        ListNode * result;
        if (index == 0) {
                result = list->firstNode;
        }
        else if (index == (list->size - 1)) {
                result = list->lastNode;
        }
        else {
                ListNode * node = list->firstNode;
                while (index > 0) {
                        node = node->next;
                        index--;
                }
                result = node;
        }
        return result;
}

ListNode * list_shift(List * list) {
        if (list->size == 0) {
                return NULL;
        }
        if (list->size == 1) {
                ListNode * node = list->firstNode;
                list->firstNode = NULL;
                list->lastNode = NULL;
                list->size = 0;
                return node;
        }
        // 2 or more nodes
        ListNode * first = list->firstNode;
        ListNode * second = first->next;
        second->previous = NULL;
        list->firstNode = second;
        list->size--;
        return first;
}

ListNode * list_pop(List * list) {
        if (list->size == 0) {
                return NULL;
        }
        if (list->size == 1) {
                ListNode * node = list->firstNode;
                list->firstNode = NULL;
                list->lastNode = NULL;
                list->size = 0;
                return node;
        }
        ListNode * last = list->lastNode;
        ListNode * penultimate = last->previous;
        penultimate->next = NULL;
        list->lastNode = penultimate;
        list->size--;
        return last;
}

void free_list_node(ListNode * node) {
        node->value = NULL;
        node->next = NULL;
        node->previous = NULL;
        free(node);
}

void free_list(List * list) {
        ListNode * node = list->firstNode;
        while (node != NULL) {
                ListNode * next = node->next;
                free_list_node(node);
                node = next;
        }
        list->firstNode = NULL;
        list->lastNode = NULL;
        free(list);
}
